package model;

import java.sql.*;
public class Test 
{
	    public static void main(String[] a) throws Exception 
	    {
	        Connection conn;
			Statement stmt;
			
			try
			{
				Class.forName("org.h2.Driver");
				conn = DriverManager.getConnection("jdbc:h2:~/test", "sa", "");
				System.out.println("Connected to database.");
				
				stmt = conn.createStatement();

				String ct = "CREATE TABLE CUSTOMER " +
				        "(CUSTOMER_ID INTEGER not NULL, " +
				        " FIRSTNAME VARCHAR(255), " +
				        " LASTNAME VARCHAR(255), " +
				        " CREDIT INTEGER)";
				
				stmt.executeUpdate(ct);
				System.out.println("Table created.");
				
				String data = "INSERT INTO CUSTOMER VALUES(1,'Peter','Schwarz',2978);" +
							  "INSERT INTO CUSTOMER VALUES(2,'Doris','Huber',3653);" +
						      "INSERT INTO CUSTOMER VALUES(3,'Emma','M�ller',6545);" +
							  "INSERT INTO CUSTOMER VALUES(4,'Petra','Fl�gel',565);" +
						      "INSERT INTO CUSTOMER VALUES(5,'Sven','Krantz',563);" +
							  "INSERT INTO CUSTOMER VALUES(6,'R�diger','Neuh�user',4515);" +
							  "INSERT INTO CUSTOMER VALUES(7,'Heinrich','Birnbaum',5612);";
				
				stmt.executeUpdate(data);
				System.out.println("Data inserted.");
				
				String select = "select * from customer";
				PreparedStatement selectPS = conn.prepareStatement(select);
				ResultSet rs = selectPS.executeQuery();
				
				while (rs.next()) 
				{
				    System.out.println("Id: " + rs.getInt("customer_id") + ", Firstname: " + rs.getString("firstname") + ", Lastname: " + rs.getString("lastname") + ", Credit: " +rs.getInt("credit"));
				}
				
				selectPS.close();
				
				conn.close();
			} 
			catch (Exception e)
			{
				System.out.println(e.getMessage());
			}      
	    }
}